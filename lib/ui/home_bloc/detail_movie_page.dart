import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';

class DetailMoviePage extends StatelessWidget {
  final Data data;

  const DetailMoviePage({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Detail Movie"),
        ),
        body: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Image.network(
                    data.i.imageUrl,
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Text(
                    data.l + " || " + data.year.toString(),
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Divider(),
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Text("Series"),
                ),
                (data.series != null)
                    ? Container(
                        padding: EdgeInsets.all(8),
                        height: 500,
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: data.series.length,
                            itemBuilder: (context, index) {
                              return Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.symmetric(horizontal: 8),
                                    width: 200,
                                    height: 150,
                                    child: Image.network(
                                      data.series[index].i.imageUrl,
                                    ),
                                  ),
                                  Container(
                                    width: 200,
                                    height: 200,
                                    child: Text(
                                      data.series[index].l,
                                    ),
                                  ),
                                ],
                              );
                            }),
                      )
                    : Container(
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        child: Text("No Series"),
                      )
              ],
            ),
          ),
        ));
  }
}
