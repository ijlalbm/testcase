import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/db/database_helper.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/models/user.dart';

class RegisterPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<RegisterPage> {
  final _emailController = TextController();
  final _userNameController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthBlocCubit(
        databaseHelper: DatabaseHelper(),
      ),
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Register',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan registrasi terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                CustomButton(
                  text: 'Register',
                  onPressed: handleRegister,
                  height: 100,
                ),
                SizedBox(
                  height: 50,
                ),
                _toLogin(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Container(
        height: 310,
        child: Column(
          children: <Widget>[
            Expanded(
              child: CustomTextFormField(
                context: context,
                label: 'Username',
                hint: 'username',
                controller: _userNameController,
                validator: (value) {
                  if (value.trim().length < 4) {
                    return 'Username must be at least 4 characters in length';
                  }
                  // Return null if the entered username is valid
                  return null;
                },
              ),
            ),
            Expanded(
              child: CustomTextFormField(
                context: context,
                controller: _emailController,
                isEmail: true,
                hint: 'Example@123.com',
                label: 'Email',
                validator: (val) {
                  final pattern =
                      new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
                  if (val != null)
                    return pattern.hasMatch(val) ? null : 'email is invalid';
                },
              ),
            ),
            Expanded(
              child: CustomTextFormField(
                context: context,
                label: 'Password',
                hint: 'password',
                controller: _passwordController,
                isObscureText: _isObscurePassword,
                suffixIcon: IconButton(
                  icon: Icon(
                    _isObscurePassword
                        ? Icons.visibility_off_outlined
                        : Icons.visibility_outlined,
                  ),
                  onPressed: () {
                    setState(() {
                      _isObscurePassword = !_isObscurePassword;
                    });
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _toLogin() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          Navigator.pop(
            context,
          );
        },
        child: RichText(
          text: TextSpan(
              text: 'Sudah punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Login',
                ),
              ]),
        ),
      ),
    );
  }

  void handleRegister() async {
    final _username = _userNameController.value;
    final _email = _emailController.value;
    final _password = _passwordController.value;
    if (formKey.currentState?.validate() == true &&
        _username != null &&
        _email != null &&
        _password != null) {
      AuthBlocCubit authBlocCubit =
          AuthBlocCubit(databaseHelper: DatabaseHelper());
      User user = User(
        userName: _username.toString(),
        email: _email.toString(),
        password: _password.toString(),
      );
      authBlocCubit.registerUser(user);
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => MyApp(),
        ),
      );
    }
  }
}
