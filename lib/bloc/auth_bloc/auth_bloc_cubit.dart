import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/db/database_helper.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  final DatabaseHelper databaseHelper;
  AuthBlocCubit({@required this.databaseHelper})
      : super(
          AuthBlocInitialState(
            databaseHelper: DatabaseHelper(),
          ),
        );

  void fetchHistoryLogin() async {
    emit(
      AuthBlocInitialState(
        databaseHelper: DatabaseHelper(),
      ),
    );
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(User user) async {
    try {
      var db = databaseHelper.getUserByEmail(user.email, user.password);
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      db.then((value) async {
        print(value);
        if (value != null) {
          emit(AuthBlocLoadingState());
          await sharedPreferences.setBool("is_logged_in", true);
          sharedPreferences.setString("user_value", value["username"]);
          emit(AuthBlocLoggedInState());
          Fluttertoast.showToast(
              msg: "Login Berhasil",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0);
        } else {
          Fluttertoast.showToast(
              msg: "User tidak terdaftar",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
          print("tidak ada user");
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void registerUser(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    try {
      databaseHelper.insertUser(user);
      sharedPreferences.setBool("is_logged_in", true);
      print(user.userName.toString());
      sharedPreferences.setString("user_value", user.userName.toString());
      emit(AuthBlocLoggedInState());
      Fluttertoast.showToast(
          msg: "Registrasi Berhasil",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      print(e);
      emit(AuthBlocErrorState(e));
    }
  }

  void addUser(User user) async {
    try {
      await databaseHelper.insertUser(user);
    } catch (e) {}
  }
}
