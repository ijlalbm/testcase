import 'package:majootestcase/models/user.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static DatabaseHelper _databaseHelper;
  static Database _database;

  DatabaseHelper._createObject();

  factory DatabaseHelper() {
    if (_databaseHelper == null) {
      _databaseHelper = DatabaseHelper._createObject();
    }

    return _databaseHelper;
  }

  static const String _tblUser = 'user';

  Future<Database> _initializeDb() async {
    var path = await getDatabasesPath();
    var db = openDatabase(
      '$path/user.db',
      onCreate: (db, version) async {
        await db.execute('''CREATE TABLE $_tblUser (
             id INTEGER PRIMARY KEY AUTOINCREMENT,
             email TEXT,
             username TEXT,
             password TEXT            
           )     
        ''');
      },
      version: 1,
    );
    return db;
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await _initializeDb();
    }

    return _database;
  }

  Future<void> insertUser(User user) async {
    final db = await database;
    await db.insert(
      _tblUser,
      user.toJson(),
    );
  }

  Future<List<User>> getUser() async {
    final db = await database;
    List<Map<String, dynamic>> results = await db.query(_tblUser);

    return results
        .map(
          (res) => User.fromJson(res),
        )
        .toList();
  }

  Future<Map> getUserByEmail(String email, String password) async {
    final db = await database;

    List<Map<String, dynamic>> results = await db.query(
      _tblUser,
      where: 'email = ? AND password = ?',
      whereArgs: [email, password],
    );

    if (results.isNotEmpty) {
      return results.first;
    } else {
      return null;
    }
  }

  // Future<void> removeLike(String id) async {
  //   final db = await database;

  //   await db.delete(
  //     _tblLike,
  //     where: 'id = ?',
  //     whereArgs: [id],
  //   );
  // }
}
